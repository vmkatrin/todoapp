//
//  String+extensions.swift
//  ToDoApp
//
//  Created by Katrin on 19.10.2021.
//

import Foundation

extension String {
    var percentEncoded: String {
        let allowedCharacters = CharacterSet(charactersIn: "!@#$%ˆ&*()_+=[]\\}{,./?><").inverted
        
        guard let encodedString = self.addingPercentEncoding(withAllowedCharacters: allowedCharacters) else {
            fatalError()
        }
        
        return encodedString
    }
}
